﻿var React = require('react');
var ReactDOM = require('react-dom');
import App from './components/App.jsx';
require('./main.scss')

ReactDOM.render(<App/>, document.getElementById('container'));
