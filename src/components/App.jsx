﻿import React from 'react';
import MessageList from './MessageList.jsx';
import ChannelList from './ChannelList.jsx';
import MessageBox from './MessageBox.jsx';
import {AppBar} from 'material-ui';


class App extends React.Component {
    constructor() {
        super();


    }

    render() {
        return (
            <div>
                <AppBar title="Chat App"/>
                <div style={{display:'flex', flexFlow: 'row wrap',
                maxWidth:1200, width:'100%', margin:'30px auto 30px'
                }}>
                    <ChannelList/>
                    <MessageList/>
                </div>
                <MessageBox></MessageBox>
            </div>
        );
    }
}

export default App;