import React from 'react';
import Channel from './Channel.jsx'
import {Card, List} from 'material-ui';

class ChannelList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            channels:[
                'Dogs',
                'Cats'
            ]
        };
    }

    render(){
        var channelNodes = this.state.channels.map((channel)=>{
            return (
                <Channel channel={channel}></Channel>
            );
        });

        return(
            <Card style={{flexGrow:1}}>
                <List>
                    {channelNodes}
                </List>
            </Card>
        );
    }
}

export default ChannelList;