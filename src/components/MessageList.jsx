import React from 'react';
import Message from './Message.jsx';
import Firebase from 'firebase';
import _ from 'lodash';
import {Card, List} from 'material-ui';


class MessageList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            messages:[]
        };

        this.firebaseRef = new Firebase('https://zerdushchatapp.firebaseio.com/messages');
        this.firebaseRef.once("value", (dataSnapshot)=>{
            var messagesVal = dataSnapshot.val();
            var messages = _(messagesVal)
                .keys()
                .map((messageKey)=>{
                    var cloned = _.clone(messagesVal[messageKey]);
                    cloned.key = messageKey;
                    return cloned;
                }).value();
            this.setState({messages:messages})

        });
    }

    render(){
        var messageNodes = this.state.messages.map((message)=>{
            return (
                <Message message={message.message}></Message>
            );
        });

        return(
            <Card style={{flexGrow:2, marginLeft:30}}>
                <List>
                    {messageNodes}
                </List>
            </Card>
        );
    }
}

export default MessageList;