import React from 'react'
import {ListItem, Avatar} from 'material-ui'

class Message extends React.Component{
    constructor(props){
        super(props);
    }

    render(){

        return (
            <ListItem leftAvatar={<Avatar src="https://irs1.4sqi.net/img/user/130x130/BQCV15JZUJRTUSXY.jpg"/> }>{this.props.message}</ListItem>
        );
    }

}

export default Message;